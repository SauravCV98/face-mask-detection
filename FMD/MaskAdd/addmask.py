from imutils import face_utils
import imutils
import dlib
import numpy as np
import argparse
import cv2
import os

# os.chdir('PATH_TO_DIR')
path = '/images'

# Initialize color [color_type] = (Blue, Green, Red)
color_blue = (239, 207, 137)
color_cyan = (255, 200, 0)
color_black = (0, 0, 0)

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="path to input image")
ap.add_argument("-p", "--prototxt", required=True, help="samples/dnn/face_detector/deploy.prototxt")
ap.add_argument("-m", "--model", required=True, help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.35, help="minimum probability to filter weak detections")
ap.add_argument("-s", "--shape-predictor", required=True, help="path to facial landmark predictor")
args = vars(ap.parse_args())

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])
predictor = dlib.shape_predictor(args["shape_predictor"])
# load the input image and construct an input blob for the image
# by resizing to a fixed 300x300 pixels and then normalizing it
image = cv2.imread(args["image"])
# gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
(h, w) = image.shape[:2]
blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))
# pass the blob through the network and obtain the detections and
# predictions
print("[INFO] computing object detections...")
net.setInput(blob)
detections = net.forward()

# loop over the detections
for i in range(0, detections.shape[2]):
    # extract the confidence (i.e., probability) associated with the
    # prediction
    confidence = detections[0, 0, i, 2]
    # filter out weak detections by ensuring the `confidence` is
    # greater than the minimum confidence
    if confidence > args["confidence"]:
        # compute the (x, y)-coordinates of the bounding box for the
        # object
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")
        rect = dlib.rectangle(int(startX), int(startY), int(endX), int(endY))
        shape = predictor(image, rect)
        # shape = face_utils.shape_to_np(shape)

        points = []
        for a in range(1, 16):
            point = [shape.part(a).x, shape.part(a).y]
            points.append(point)
        # print(points)

        # Coordinates for the additional 3 points for wide, high coverage mask - in sequence
        mask_a = [((shape.part(42).x), (shape.part(15).y)),
                  ((shape.part(27).x), (shape.part(27).y)),
                  ((shape.part(39).x), (shape.part(1).y))]

        # Coordinates for the additional point for wide, medium coverage mask - in sequence
        mask_c = [((shape.part(29).x), (shape.part(29).y))]

        # Coordinates for the additional 5 points for wide, low coverage mask (lower nose points) - in sequence
        mask_e = [((shape.part(35).x), (shape.part(35).y)),
                  ((shape.part(34).x), (shape.part(34).y)),
                  ((shape.part(33).x), (shape.part(33).y)),
                  ((shape.part(32).x), (shape.part(32).y)),
                  ((shape.part(31).x), (shape.part(31).y))]

        fmask_a = points + mask_a
        fmask_c = points + mask_c
        fmask_e = points + mask_e

        # mask_type = {1: fmask_a, 2: fmask_c, 3: fmask_e}
        # mask_type[choice2]

        # Using Python OpenCV – cv2.polylines() method to draw mask outline for [mask_type]:
        # fmask_a = wide, high coverage mask,
        # fmask_c = wide, medium coverage mask,
        # fmask_e  = wide, low coverage mask

        fmask_a = np.array(fmask_a, dtype=np.int32)
        fmask_c = np.array(fmask_c, dtype=np.int32)
        fmask_e = np.array(fmask_e, dtype=np.int32)

        mask_type = {1: fmask_a, 2: fmask_c, 3: fmask_e}
        mask_type[2]

        # change parameter [mask_type] and color_type for various combination
        img2 = cv2.polylines(image, [mask_type[2]], True, color_black, thickness=2, lineType=cv2.LINE_8)

        # Using Python OpenCV – cv2.fillPoly() method to fill mask
        # change parameter [mask_type] and color_type for various combination
        img3 = cv2.fillPoly(img2, [mask_type[2]], color_black, lineType=cv2.LINE_AA)

    # cv2.imshow("image with mask outline", img2)
cv2.imshow("image with mask", img3)

# Save the output file for testing
outputNameofImage = "output/imagetest.jpg"
print("Saving output image to", outputNameofImage)
cv2.imwrite(outputNameofImage, img3)

##y = startY - 10 if startY - 10 > 10 else startY + 10
##cv2.rectangle(image, (startX, startY), (endX, endY), (0, 0, 255), 2)

# draw the bounding box of the face along with the associated
# probability
# text = "{:.2f}%".format(confidence * 100)
# y = startY - 10 if startY - 10 > 10 else startY + 10
# cv2.rectangle(image, (startX, startY), (endX, endY), (0, 0, 255), 2)

# loop over the (x, y)-coordinates for the facial landmarks
# and draw them on the image
##for (x, y) in shape:
##cv2.circle(image, (x, y), 1, (0, 0, 255), -1)

# #   (x, y, w, h) = face_utils.rect_to_bb(rect)
#   cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
#   # show the face number
#   cv2.putText(image, "Face #{}".format(i + 1), (x - 10, y - 10),
#              cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

# cv2.putText(image, text, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
# show the output image
# cv2.imshow("Output", image)
cv2.waitKey(0)
